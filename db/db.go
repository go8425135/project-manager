package db

import (
	"database/sql"
	"log"
    _ "github.com/mattn/go-sqlite3"
)

var Data *sql.DB = nil;

func GetDB() *sql.DB {
	var err error;
	if Data == nil {
		Data, err = sql.Open("sqlite3", "db/projects.db")
		Data.SetMaxOpenConns(1)
		if err != nil {
			log.Fatal(err)
		}
	}
	return Data;
}
