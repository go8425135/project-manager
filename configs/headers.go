package configs

import(
	"net/http"
)


func SetHeaders(w http.ResponseWriter){
	w.Header().Set("Access-Control-Allow-Origin","*")
	w.Header().Set("Access-Control-Allow-Headers","Content-Type")
	w.Header().Set("Content-Type","application/json")
}
