# Project Manager

## Goal

The goal is to create a micro-service based bug tracker written in Go.
Hopefully I will learn more about Go as a backend for webservices and for
implementing microservices.

## Design

Listed below are db schemas/separate services

### Projects

Project objects will contain:

Id
admin id
Project Name
Description

### Items

Item objects will contain:

Id
Project -> project it belongs to  
Type -> bug, feature, refactor  
Description  
State -> in progress, backlog, review, complete
Priority -> sorted based off of number of items (lower number is more important)

### Comments

Comment objects will contain:

Id
Item Id
User Id
Comment

### User

User objects will contain:

Id
User Name
Full Name
password

