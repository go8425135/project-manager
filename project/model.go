package project

import (
	"database/sql"
	"log"
)

func Create(r *sql.DB, proj *Project) int {
	query := "INSERT INTO project (admin,name,description) VALUES (?,?,?)"
	_, err := r.Exec(query, proj.GetAdmin(), proj.GetName(), proj.GetDescription())
	if err != nil {
		log.Fatal(err)
	}
	return 0
}

func Read(r *sql.DB, id int) Project {
	query := "SELECT admin,name,description FROM project WHERE id=?"
	res := r.QueryRow(query, id)

	var admin int
	var name string
	var description string
	var proj Project

	err := res.Scan(&admin, &name, &description)
	if err != nil {
		log.Fatal(err)
	}

	proj.
		SetId(id).
		SetAdmin(admin).
		SetName(name).
		SetDescription(description)

	return proj
}

func ReadAll(r *sql.DB) []Project {
	query := "SELECT * FROM project"
	rows, err := r.Query(query)

	var projects []Project

	for rows.Next() {
		var proj Project
		if err := rows.Scan(&proj.Id, &proj.Admin, &proj.Name,
			&proj.Description); err != nil {
			if err != nil {
				log.Fatal(err)
			}
		}
		projects = append(projects, proj)
	}

	if err != nil {
		log.Fatal(err)
	}
	return projects
}

func Update(r *sql.DB, proj *Project, id int) {
	query := "UPDATE project SET admin=?,name=?,description=? WHERE id=?"
	_, err := r.Exec(query, proj.GetAdmin(), proj.GetName(), proj.GetDescription(), id)
	if err != nil {
		log.Fatal(err)
	}
}

func Delete(r *sql.DB, id int) {
	query := "DELETE FROM project WHERE id=?"
	_, err := r.Exec(query, id)
	if err != nil {
		log.Fatal(err)
	}
}
