package project

import (
	"encoding/json"
	"fmt"
)

type Project struct {
	Id          int    `json:"id"`
	Admin       int    `json:"admin"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Projects interface {
	GetId() int
	GetAdmin() int
	GetName() string
	GetDescription() string

	SetId() *Project
	SetAdmin() *Project
	SetName() *Project
	SetDescription() *Project

	ToString() string
}

func (p *Project) GetId() int {
	return p.Id
}

func (p *Project) GetAdmin() int {
	return p.Admin
}

func (p *Project) GetName() string {
	return p.Name
}

func (p *Project) GetDescription() string {
	return p.Description
}

func (p *Project) SetId(id int) *Project {
	p.Id = id
	return p
}

func (p *Project) SetAdmin(id int) *Project {
	p.Admin = id
	return p
}

func (p *Project) SetName(name string) *Project {
	p.Name = name
	return p
}

func (p *Project) SetDescription(desc string) *Project {
	p.Description = desc
	return p
}

func (p *Project) ToJSON() string {
	byteArray, err := json.Marshal(p)
	if err != nil {
		fmt.Print(err)
	}
	return string(byteArray)
}

func FromJSON(s string) Project {
	var proj Project
	err := json.Unmarshal([]byte(s), &proj)
	if err != nil {
		fmt.Print(err)
	}
	return proj
}
