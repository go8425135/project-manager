package project

import (
	"fmt"
	//"io"
	//"log"
	"encoding/json"
	"net/http"
	"projectmgr/configs"
	"projectmgr/db"
)

var DB = db.GetDB()

func ShowProjects(w http.ResponseWriter, r *http.Request) {
	configs.SetHeaders(w)
	fmt.Printf("got /users request\n")

	var projects []Project = (ReadAll(DB))

	err := json.NewEncoder(w).Encode(projects)
	if err != nil {
		fmt.Println("Unable to process request.")
	}

}
