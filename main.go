package main

import (
	"log"
	"net/http"
	"projectmgr/user"
	"projectmgr/project"
	"projectmgr/items"
)

func main() {
	// Routes

	// user routes
	http.HandleFunc("/getusers", user.ShowUsers)
	http.HandleFunc("/getuser", user.ShowUser)
	http.HandleFunc("/newuser", user.NewUser)
	
	// project routes
	http.HandleFunc("/getprojects", project.ShowProjects)

	// item routes
	http.HandleFunc("/getItems", item.ItemsForProj)
	
	// Startup
	err := http.ListenAndServe(":3333", nil)
	if err != nil {
		log.Fatalln("Could not start server!!")
	}
}
