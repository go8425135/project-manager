package user

import (
	"fmt"
	"io"
	//"log"
	"net/http"
	"projectmgr/db"
	"projectmgr/configs"
	"encoding/json"
)

var DB = db.GetDB()

func ShowUser(w http.ResponseWriter, r *http.Request){
	configs.SetHeaders(w)
	fmt.Printf("got /user request\n")

	req, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Unable to process POST request.")
	}

	raw := string(req)
	fmt.Println(raw)

	var usr User
	err = json.Unmarshal(req, &usr)
	if err != nil {

		fmt.Println(err);
		fmt.Println("Bad request")
		str := "{'err':'Malformed Request'}"

		_, err = io.WriteString(w, str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	} else {

		display := (Read(DB, usr.GetId()))
		str := display.ToJSON()

		err := json.NewEncoder(w).Encode(str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	}
}

func ShowUsers(w http.ResponseWriter, r *http.Request){
	configs.SetHeaders(w)
	fmt.Printf("got /users request\n")

	var usrs []User = (ReadAll(DB))

	err := json.NewEncoder(w).Encode(usrs)
	if err != nil {
		fmt.Println("Unable to process request.")
	}

}

func NewUser(w http.ResponseWriter, r *http.Request){
	configs.SetHeaders(w)
	fmt.Printf("got /newUser request\n")

	req, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Unable to process POST request.")
	}

	raw := string(req)
	fmt.Println(raw)

	var usr User
	err = json.Unmarshal(req, &usr)
	if err != nil {

		fmt.Println(err);
		fmt.Println("Bad request")
		str := "{'err':'Malformed Request'}"

		_, err = io.WriteString(w, str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	} else {

		newId := Create(DB, &usr)

		str := fmt.Sprintf(`{"id":"%d"}`, newId)

		err := json.NewEncoder(w).Encode(str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	}
}
