package user

import (
	"crypto/sha512"
	"fmt"
	"os"
	"encoding/json"
)

type User struct {
	Id       int `json:"id"`
	FullName string `json:"fullName"`
	UserName string `json:"userName"`
	Password []byte `json:"password"`
}

type Users interface {
	ToString() string

	GetId() int
	GetFullName() string
	GetUserName() string
	GetPassword() []byte

	SetId() *User
	SetFullName() *User
	SetUserName() *User
	SetPassword() *User
}

func (u *User) SetId(id int) *User {
	u.Id = id
	return u
}

func (u *User) SetFullName(name string) *User {
	u.FullName = name
	return u
}

func (u *User) SetUserName(name string) *User {
	u.UserName = name
	return u
}

func (u *User) SetPassword(passwd string) *User {
	var basekey []byte = []byte(passwd)
	var key []byte

	hasher := sha512.New()
	_, err := hasher.Write(basekey)
	if err != nil {
		fmt.Println("Hashing Error")
		os.Exit(512)
	}
	key = hasher.Sum(nil)

	u.Password = key
	return u
}

func (u *User) GetId() int {
	return u.Id
}

func (u *User) GetFullName() string {
	return u.FullName
}

func (u *User) GetUserName() string {
	return u.UserName
}

func (u *User) GetPassword() []byte {
	return u.Password
}

func (u *User) ToJSON() string {
	byteArray, err := json.Marshal(u)
	if err != nil {
		fmt.Print(err)
	}
	return string(byteArray)
}

func FromJSON(s string) User {
	var usr User 
	err := json.Unmarshal([]byte(s), &usr)
	if err != nil {
		fmt.Print(err)
	}
	return  usr
}
