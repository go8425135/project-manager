package user

import (
	"database/sql"
	"log"
)

func Create(r *sql.DB, usr *User) int64 {
	query := "INSERT INTO users (username,full_name,password) VALUES (?,?,?)"
	res, err := r.Exec(query, usr.GetUserName(), usr.GetFullName(), usr.GetPassword())
	if err != nil {
		log.Fatal(err)
	}

	id,err := res.LastInsertId();
	if err != nil {
		log.Fatal(err)
	}
	return id;
}

func Read(r *sql.DB, id int) User {
	query := "SELECT id,username,full_name FROM users WHERE id=?"
	res := r.QueryRow(query, id)

	var usr User
	err := res.Scan(&usr.Id, &usr.UserName, &usr.FullName)

	if err != nil {
		log.Fatal(err)
	}

	return usr
}

func ReadUnsafe(r *sql.DB, id int) User {
	query := "SELECT * FROM users WHERE id=?"
	res := r.QueryRow(query, id)

	var usr User
	err := res.Scan(&usr.Id, &usr.UserName, &usr.FullName, &usr.Password)

	if err != nil {
		log.Fatal(err)
	}

	return usr
}

func ReadAll(r *sql.DB) []User {
	query := "SELECT id,username,full_name FROM users"
	rows, err := r.Query(query)
	
	var users []User

	for rows.Next() {
		var usr User
		if err := rows.Scan(&usr.Id, &usr.UserName, &usr.FullName); err != nil {
			if err != nil {
				log.Fatal(err)
			}
		}
		users = append(users, usr)
	}

	if err != nil {
		log.Fatal(err)
	}
	return users
}

func Update(r *sql.DB, usr *User, id int) {
	query := "UPDATE users SET username=?,full_name=?,password=? WHERE id=?"
	_, err := r.Exec(query, usr.GetUserName(), usr.GetFullName(), usr.GetPassword(), usr.GetId())
	if err != nil {
		log.Fatal(err)
	}
}

func Delete(r *sql.DB, usr *User) int {
	query := "DELETE FROM users WHERE id=?"
	_, err := r.Exec(query, usr.GetId())
	if err != nil {
		log.Fatal(err)
	}
	return 0
}
