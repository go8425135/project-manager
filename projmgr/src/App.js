import React, {Component} from 'react'
import Header from './Header'
import Nav from './Nav'
import UsersCard from './mainCards/UsersCard'
import Users from './pages/Users'
import NotFound from './pages/NotFound'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

class App extends Component {

	constructor(){
		super();
        let colors = document.createElement('script');
        colors.src = './color-theme.js';
        document.body.appendChild(colors);
	}

    render() {
        return (
			<Router>
			  <div>
				{/* end mobile header */}
				<Header />           
				<div className="flex@md">

				<Nav />
				  
				  <main className="position-relative z-index-1 flex-grow min-height-100vh position-sticky@md top-0@md height-100vh@md overflow-auto@md sidebar-loaded:show">
					{/* start main content */}

					{/* start desktop main header */}
					<header className="position-sticky top-0 z-index-header bg bg-opacity-80% border-bottom border-alpha backdrop-blur-10 display@md">
					  <div className="height-60 flex items-center justify-between padding-x-component">
						<div className="search-input search-input--icon-left position-relative width-100% max-width-xxxxs autocomplete js-autocomplete" data-autocomplete-dropdown-visible-class="autocomplete--results-visible">
						  <form action="search-results.html">
							<input autoComplete="off" className="search-input__input form-control js-autocomplete__input" type="search" name="search-input" id="search-input" placeholder="Search..." aria-label="Search" />
						  
							<button className="search-input__btn">
							  <svg className="icon" viewBox="0 0 16 16">
								<title>Submit</title>
								<circle cx="6.5" cy="6.5" r="4.5" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/><line x1="14" y1="14" x2="10" y2="10" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
							  </svg>
							</button>
						  </form>

						  <span className="search-input__shortcut" aria-hidden="true">/</span>

						  <div className="autocomplete__loader position-absolute top-0 right-0 padding-right-xxs height-100% flex items-center" aria-hidden="true">
							<div className="circle-loader circle-loader--v1">
							  <div className="circle-loader__circle"></div>
							</div>
						  </div>

						  <div className="autocomplete__results js-autocomplete__results">
							<ul id="autocomplete1" className="autocomplete__list js-autocomplete__list">
							  <li className="autocomplete__item js-autocomplete__item is-hidden">
								<a data-autocomplete-url className="text-decoration-none color-contrast-higher block padding-y-xs padding-x-sm autocomplete__link">
								  <span className="block" data-autocomplete-label></span>
								  <span className="block text-xs color-contrast-medium margin-top-xxxxs" data-autocomplete-category></span>
								</a>
							  </li>
							</ul>
						  </div>

						  <p className="sr-only" aria-live="polite" aria-atomic="true"><span className="js-autocomplete__aria-results">0</span> results found.</p>
						</div>

						<button className="reset user-menu-control" aria-controls="user-menu" aria-label="Toggle user menu">
						  <figure className="user-menu-control__img-wrapper radius-50%">
							<img className="user-menu-control__img" src="assets/img/author-img-1.jpg" alt="User picture" />
						  </figure>
						
						  <div className="margin-x-xs user-menu__meta">
							<p className="user-menu__meta-title text-sm line-height-1 padding-y-xxxxs font-semibold color-contrast-higher text-truncate">Emily Ewing</p>
							<p className="text-xs color-contrast-medium line-height-1 padding-bottom-xxxxs">Admin</p>
						  </div>
						
						  <svg className="icon icon--xxs" aria-hidden="true" viewBox="0 0 12 12">
							<polyline points="1 4 6 9 11 4" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/>
						  </svg>
						</button>
						
						<menu id="user-menu" className="menu js-menu">
						  <li role="menuitem">
							<a className="menu__content js-menu__content" href="settings.html">
							  <svg className="icon menu__icon" aria-hidden="true" viewBox="0 0 16 16"><g fill="currentColor"><path d="M15.135,6.784c-1.303-0.326-1.921-1.818-1.23-2.969c0.322-0.536,0.225-0.998-0.094-1.316l-0.31-0.31 c-0.318-0.318-0.78-0.415-1.316-0.094c-1.152,0.691-2.644,0.073-2.969-1.23C9.065,0.258,8.669,0,8.219,0H7.781 c-0.45,0-0.845,0.258-0.997,0.865c-0.326,1.303-1.818,1.921-2.969,1.23C3.279,1.773,2.816,1.87,2.498,2.188l-0.31,0.31 C1.87,2.816,1.773,3.279,2.095,3.815c0.691,1.152,0.073,2.644-1.23,2.969C0.26,6.935,0,7.33,0,7.781v0.438 c0,0.45,0.258,0.845,0.865,0.997c1.303,0.326,1.921,1.818,1.23,2.969c-0.322,0.536-0.225,0.998,0.094,1.316l0.31,0.31 c0.319,0.319,0.782,0.415,1.316,0.094c1.152-0.691,2.644-0.073,2.969,1.23C6.935,15.742,7.331,16,7.781,16h0.438 c0.45,0,0.845-0.258,0.997-0.865c0.326-1.303,1.818-1.921,2.969-1.23c0.535,0.321,0.997,0.225,1.316-0.094l0.31-0.31 c0.318-0.318,0.415-0.78,0.094-1.316c-0.691-1.152-0.073-2.644,1.23-2.969C15.742,9.065,16,8.669,16,8.219V7.781 C16,7.33,15.74,6.935,15.135,6.784z M8,11c-1.657,0-3-1.343-3-3s1.343-3,3-3s3,1.343,3,3S9.657,11,8,11z"></path></g></svg>
							  <span>Settings</span>
							</a>
						  </li>
						
						  <li role="menuitem">
							<a className="menu__content js-menu__content" href="users.html">
							  <svg className="icon menu__icon" aria-hidden="true" viewBox="0 0 16 16"><circle cx="10.5" cy="2.5" r="2.5"/><circle cx="5.5" cy="6.5" r="2.5"/><path d="M15.826,10.124A5.455,5.455,0,0,0,9.46,6.107,3.932,3.932,0,0,1,9.5,6.5,3.97,3.97,0,0,1,8.452,9.176,6.963,6.963,0,0,1,11.539,12h2.829a1.5,1.5,0,0,0,1.458-1.876Z"/><path d="M10.826,14.124a5.5,5.5,0,0,0-10.652,0A1.5,1.5,0,0,0,1.632,16H9.368a1.5,1.5,0,0,0,1.458-1.876Z"/></svg>
							  <span>Users</span>
							</a>
						  </li>
						
						  <li className="menu__separator" role="separator"></li>
						
						  <li role="menuitem">
							<a className="menu__content js-menu__content" href="login.html">Logout</a>
						  </li>
						</menu>
					  </div>
					</header>
					{/* end desktop main header */}

					{/* start container */}
					<div className="container max-width-lg padding-y-lg">
					  <ul className="grid gap-lg">
						<Routes>
							<Route exact path="/" element={<UsersCard />} />
							<Route exact path="/users" element={<Users />} />
							<Route exact path="*" element={<NotFound />} />
						</Routes>
					  </ul>
					</div>
					{/* end container */}

					{/* end main content */}
				  </main>
				</div>
			  </div>
			</Router>
        );
      }
}

export default App;
