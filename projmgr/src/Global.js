function Global(){
	return (
		<div>
			<script src="color-theme.js"></script>
			<script src="scripts.js" defer></script>
		</div>
	);
}
export default Global;
