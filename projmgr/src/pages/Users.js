import React, { Component } from 'react'
import $ from 'axios'
import Content from '../Content'
import { SERVER } from '../lib'

export default class Users extends Component {

	state = {
		title: "Users",
		content: <p> testing user </p>,
		showMenu: false,
		users: [],
		activeUsers: [],
		page: 0,
		pages: 1,
		results: 1,
	}

	title = "Add User"

	updateMenu = () => {
		this.setState({ showMenu: !this.state.showMenu });
	}

	getActiveUsers = (arr) => {
		return arr.slice(this.state.page * 7, (this.state.page + 1) * 7);
	}

	pageForward = async () => {
		await this.setState({
			page: this.state.page + 1,
			activeUsers: this.getActiveUsers(this.state.users)
		});
		console.log(this.state);
		document.querySelector("#pageNumber").value = this.state.page + 1;
	}

	pageBack = async () => {
		await this.setState({ page: this.state.page - 1 });
		document.querySelector("#pageNumber").value = this.state.page + 1;
		//let activeUsers = this.state.users.slice(this.state.page * 7, this.state.page * 7 + 7);
	}

	notify() {
		let evt = new CustomEvent('openModal');
		document.querySelector('#modal-notify').dispatchEvent(evt);
	}

	handleSubmit = async () => {
		const user = {
			fullName: document.newUser.fullName.value,
			userName: document.newUser.userName.value,
			password: document.newUser.password.value,
		};
		let res = await $.post(SERVER + "/newuser", user);
		console.log(JSON.parse(res.data).id);
		this.getUsers();
	}

	form =
		<form className="grid gap-md" name="newUser">
			<input name="fullName" type='text' placeholder='Full Name' className='form-control col' />
			<input name="userName" type='text' placeholder='User Name' className='form-control col' />
			<input name="password" type='Password' placeholder='Password' className='form-control col' />
		</form>

	async getUsers() {
		const url = SERVER + "/getusers";

		const res = await $.get(url);
		const resp = res.data;
		console.log(resp)

		const titles = ["Id", "Full name", "Username", "Actions"]
			.map(title => <th key={title} className="int-table__cell int-table__cell--th text-left">{title}</th>);

		const users = resp.map(user =>
			<tr key={user.id} className="int-table__row" data-order="0">
				<td className="int-table__cell max-width-xxxxs">{user.id}</td>
				<td className="int-table__cell">{user.fullName}</td>
				<td className="int-table__cell">{user.userName}</td>
				<td className="int-table__cell text-right">
					<button onClick={this.updateMenu} className="reset float-left int-table__menu-btn margin-left-auto js-tab-focus" data-label="Edit row" aria-controls="menu-example" aria-expanded="false" aria-haspopup="true" style={{ outline: "none" }}>
						<svg className="icon" viewBox="0 0 16 16"><circle cx="8" cy="7.5" r="1.5"></circle><circle cx="1.5" cy="7.5" r="1.5"></circle><circle cx="14.5" cy="7.5" r="1.5"></circle></svg>
					</button>
				</td>
			</tr>
		);

		let activeUsers = this.getActiveUsers(users);

		this.setState({
			users,
			activeUsers,
			results: resp.length,
			pages: Math.ceil(resp.length / 7)
		});

		setTimeout(() => console.log(this.state), 1000);

		const menu = <menu id="menu-example" className={this.state.showMenu ? "menu js-menu menu--is-visible" : "menu js-menu menu--is-hidden"} data-scrollable-element=".js-app-ui__body" style={{ right: "56.421875px", top: "291.984375px", maxHeight: "447px" }}>
			<li key="edit" role="menuitem">
				<span className="menu__content js-menu__content" tabIndex="0">
					<svg className="icon menu__icon" aria-hidden="true" viewBox="0 0 12 12"><path d="M10.121.293a1,1,0,0,0-1.414,0L1,8,0,12l4-1,7.707-7.707a1,1,0,0,0,0-1.414Z"></path></svg>
					<span>Edit</span>
				</span>
			</li>
		</menu>;

		const modal =
			<div id="modal-notify" className="modal modal--animate-scale mc4-flex mc4-flex-center mc4-bg-black mc4-bg-opacity-90% mc4-padding-md js-modal">
				<div className="modal__content mc4-width-100% mc4-max-width-xs mc4-max-height-100% mc4-overflow-auto mc4-bg mc4-radius-md mc4-inner-glow mc4-shadow-md" role="alertdialog" aria-labelledby="modal-title-1" aria-describedby="modal-description-1">
					<header className="mc4-bg-contrast-lower mc4-bg-opacity-50% mc4-padding-y-sm mc4-padding-x-md mc4-flex mc4-items-center mc4-justify-between">
						<h1 id="notify-title" className="mc4-text-truncate mc4-text-md">{this.title}</h1>

						<button className="modal__close-btn modal__close-btn--inner mc4-hide@md js-modal__close js-tab-focus">
							<svg className="mc4-icon mc4-icon--xs" viewBox="0 0 16 16">
								<title>Close modal window</title>
								<g strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10">
									<line x1="13.5" y1="2.5" x2="2.5" y2="13.5"></line>
									<line x1="2.5" y1="2.5" x2="13.5" y2="13.5"></line>
								</g>
							</svg>
						</button>
					</header>

					<div className="mc4-padding-y-sm mc4-padding-x-md">
						<div className="mc4-text-component">
							{this.form}
						</div>
					</div>

					<footer className="mc4-padding-md">
						<div className="mc4-flex mc4-justify-end mc4-gap-xs">
							<button className="mc4-btn mc4-btn--primary js-modal__close">Cancel</button>
							<button onClick={this.handleSubmit} className="mc4-btn btn--primary js-modal__close">Submit</button>
						</div>
					</footer>
				</div>

				<button className="modal__close-btn modal__close-btn--outer mc4-display@md js-modal__close js-tab-focus">
					<svg className="mc4-icon mc4-icon--sm" viewBox="0 0 24 24">
						<title>Close modal window</title>
						<g fill="none" stroke="currentColor" strokeMiterlimit="10" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
							<line x1="3" y1="3" x2="21" y2="21" />
							<line x1="21" y1="3" x2="3" y2="21" />
						</g>
					</svg>
				</button>
			</div>

		let content = (titles, rows, results, pages, page) => (<div className="int-table__inner">
			<button className="btn btn--primary" onClick={() => this.notify()} >Add User +</button>
			<table className="int-table__table" aria-label="Interactive table example">
				<thead className="int-table__header js-int-table__header">
					<tr className="int-table__row">
						{titles}
					</tr>
				</thead>

				<tbody className="int-table__body js-int-table__body">
					{rows}
				</tbody>
			</table>
			<div className="flex items-center justify-between padding-top-sm">
				<p className="text-sm">{results} results</p>

				<nav className="pagination text-sm" aria-label="Pagination">
					<ul className="pagination__list flex flex-wrap gap-xxxs">
						<li onClick={this.pageBack}>
							<span className="pagination__item">
								<svg className="icon" viewBox="0 0 16 16"><title>Go to previous page</title><g strokeWidth="1.5" stroke="currentColor"><polyline fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="9.5,3.5 5,8 9.5,12.5 "></polyline></g></svg>
							</span>
						</li>

						<li>
							<span className="pagination__jumper flex items-center">
								<input aria-label="Page number" className="form-control" type="text" id="pageNumber" name="pageNumber" defaultValue={page} />
								<em>of {pages}</em>
							</span>
						</li>

						<li onClick={this.pageForward}>
							<span className="pagination__item">
								<svg className="icon" viewBox="0 0 16 16"><title>Go to next page</title><g strokeWidth="1.5" stroke="currentColor"><polyline fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="6.5,3.5 11,8 6.5,12.5 "></polyline></g></svg>
							</span>
						</li>
					</ul>
				</nav>
			</div>
			{menu}
			{modal}
		</div>);

		this.setState({ content: content(titles, this.state.activeUsers, this.state.results, this.state.pages, this.state.page + 1) });

	}

	componentDidMount() {
		this.getUsers();
		this.setState({
			users: this.state.users,
			activeUsers: this.getActiveUsers(this.state.users),
		});
	}

	render() {
		return (<Content title={this.state.title} content={this.state.content} />)
	}
}
