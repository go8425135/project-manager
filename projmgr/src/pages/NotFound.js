import React, {Component} from 'react'
import Content from '../Content'

export default class NotFound extends Component {
	state = {
		title: "404 Page Not Found!",
		content: <><p>404 Page Not Found!</p><p>Sorry, no page at this address.</p></>
	}

	render(){
		return (
			<Content title={this.state.title} content={this.state.content} />
		);
	}
}
