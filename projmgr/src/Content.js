import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class Content extends Component {

	run(){
		if(document.querySelector("#scriptme"))
			document.querySelector("#scriptme").remove()
        let scripts = document.createElement('script');
		scripts.id="scriptme"
        scripts.src = './scripts.js';
		scripts.defer = "defer"
        document.body.appendChild(scripts);
	}

	render(){
		let link = "";
		if(this.props.link){
		  link = <p className="float-right"><Link to={this.props.link.to}>{this.props.link.name}</Link></p>
		}

		return (
			<li>
			  <header className="margin-bottom-sm">
				<div className="flex flex-column gap-xxs flex-row@sm justify-between@sm items-baseline@sm">
				  <h2 className="text-lg">{this.props.title}</h2>
				  {link}
				</div>
			  </header>
	  
			  <div className="card padding-md">
				{this.props.content}	
			  </div>
			{this.run()}
			</li>
		);
	}
}

export default Content
