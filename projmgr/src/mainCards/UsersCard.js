import React, { Component } from 'react'
import $ from 'axios'
import { SERVER } from '../lib'
import Content from '../Content'

class UsersCard extends Component {
	state = {
		title: "Users",
		content: <p> testing user </p>,
		link: {
			name: "Go to Users ->",
			to: "/users"
		}
	}

	async getUser() {
		const url = SERVER + "/getusers";

		const res = await $.get(url);
		console.log(res)
		const resp = res.data;
		console.log(resp)

		let content = resp.map(i => <p>Username: {i.userName} Full Name: {i.fullName}</p>)

		this.setState({ content });

	}

	componentDidMount() {
		this.getUser();
	}

	render() {
		return (<Content title={this.state.title} content={this.state.content} link={this.state.link} />)
	}
}

export default UsersCard;
