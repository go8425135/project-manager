import React, {Component} from 'react'

class Nav extends Component {
	render() {
		return (
		  <aside id="cms-main-sidebar" className="sidebar sidebar--static@md js-sidebar" data-static-class="position-relative z-index-2 bg-dark flex flex-column">
			<div className="sidebar__panel flex-grow flex flex-column">
			  {/* start sidebar (mobile) header */}
			  <header className="sidebar__header bg padding-y-sm padding-left-md padding-right-sm border-bottom z-index-2">
				<h1 className="text-md text-truncate" id="sidebar-title">Menu</h1>
		  
				<button className="reset sidebar__close-btn js-sidebar__close-btn js-tab-focus">
				  <svg className="icon icon--xs" viewBox="0 0 16 16"><title>Close panel</title><g strokeWidth="2" stroke="currentColor" fill="none" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10"><line x1="13.5" y1="2.5" x2="2.5" y2="13.5"></line><line x1="2.5" y1="2.5" x2="13.5" y2="13.5"></line></g></svg>
				</button>
			  </header>
			  {/* end sidebar (mobile) header */}

			  {/* start sidebar (desktop) header */}
			  <header className="display@md padding-x-xs margin-bottom-lg">
				<div className="flex items-center justify-between height-60 padding-right-xs padding-left-xxxs">
				  <a className="block size-40 hover:reduce-opacity" href="index.html">
					<svg className="block" viewBox="0 0 40 40">
					  <circle fill="var(--color-contrast-higher)" cx="20" cy="20" r="20"/>
					  <path d="M12.64,20.564a6.437,6.437,0,0,0-4.4,1.388S10,24.2,12.133,24.475a6.486,6.486,0,0,0,3.625-.846S14.455,20.8,12.64,20.564Z" fill="var(--color-bg)"/>
					  <path d="M22.036,13.407a7.041,7.041,0,0,0-1.111-3.88s-3,1.562-3.152,3.54a6.978,6.978,0,0,0,1.739,4.688S21.851,15.73,22.036,13.407Z" fill="var(--color-bg)"/>
					  <path d="M29.048,26.433a7.624,7.624,0,0,0-.321-4.122c-1.052-2.448-4.326-3.784-4.326-3.784a7.973,7.973,0,0,0-.164,5.713A3.294,3.294,0,0,0,25.451,25.6,16.016,16.016,0,0,1,14.758,10.527v-1h-2v1A17.988,17.988,0,0,0,21.19,25.746a5.859,5.859,0,0,0-2.433-.151,8.093,8.093,0,0,0-4,2.352s2.6,2.883,4.846,2.49a7.889,7.889,0,0,0,4.627-3.153,17.885,17.885,0,0,0,6.527,1.243h1v-2h-1A16.094,16.094,0,0,1,29.048,26.433Z" fill="var(--color-bg)"/>
					</svg>
				  </a>
		
				  <a className="inline-flex color-contrast-high hover:reduce-opacity js-tooltip-trigger" title="Launch website" href="https://codyhouse.co/template/nettuno" target="_blank">
					<svg className="icon icon--xs" viewBox="0 0 16 16">
					  <g fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2">
						<path d="M15,12v1.5A1.5,1.5,0,0,1,13.5,15H2.5A1.5,1.5,0,0,1,1,13.5V2.5A1.5,1.5,0,0,1,2.5,1H4" />
						<polyline points="8 1 15 1 15 8" />
						<line x1="15" y1="1" x2="7" y2="9" />
					  </g>
					</svg>
				  </a>
				</div>
			  </header>
			  {/* end sidebar (desktop) header */}
		
			  {/* start sidenav content */}
			  <div className="position-relative z-index-1">
				<nav className="sidenav-v4 padding-x-xs padding-bottom-xs js-sidenav-v4">
				  <ul>
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="index.html" aria-current="page">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M16 12v7H4v-7"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 10l9-9 9 9"></path>
							<path d="M10 14a2 2 0 0 1 2 2v2H8v-2a2 2 0 0 1 2-2z"></path>
						  </g>
						</svg>
		
						<span>Overview</span>
					  </a>
					</li>
		
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="notifications.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<path d="M10 20a2 2 0 0 1-2-2h4a2 2 0 0 1-2 2z"></path>
							<path d="M19 15a3 3 0 0 1-3-3V7a6 6 0 0 0-6-6 6 6 0 0 0-6 6v5a3 3 0 0 1-3 3h18z" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path>
						  </g>
						</svg>
		
						<span>Notifications</span>
						
						<span className="sidenav-v4__notification-marker">8 <i className="sr-only">notifications</i></span>
					  </a>
					</li>
		
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="articles.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<path d="M3 1h10l5 5v12a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path>
							<path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 1l5 5h-5V1z"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 6h2"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 10h8"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 14h8"></path>
						  </g>
						</svg>
		
						<span>Articles</span>
		  
						<svg className="sidenav-v4__arrow-icon icon margin-left-auto" viewBox="0 0 20 20">
						  <g className="icon__group" fill="none" stroke="currentColor" strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round">
							<line x1="3" y1="3" x2="17" y2="17" />
							<line x1="17" y1="3" x2="3" y2="17" />
						  </g>
						</svg>
					  </a>
				
					  <ul className="sidenav-v4__sub-list">
						<li>
						  <a className="sidenav-v4__sub-link" href="articles.html">All Articles</a>
						</li>
						
						<li>
						  <a className="sidenav-v4__sub-link" href="new-article.html">Add New</a>
						</li>
						
						<li>
						  <a className="sidenav-v4__sub-link" href="categories.html">Categories</a>
						</li>
					  </ul>
					</li>

					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="reports.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4.231 11L7 5l6 5 6-8"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 18l1.385-3"></path>
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 11h4l8 6 6-5"></path>
						  </g>
						</svg>
		
						<span>Reports</span>
					  </a>
					</li>
		
					<li className="sidenav-v4__separator" role="presentation"><span></span></li>
		
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="assets.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<rect x="2" y="2" width="16" height="16" rx="2" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></rect>
							<path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 14l6-6 2 6H6z"></path><circle cx="6.5" cy="6.5" r="1.5"></circle>
						  </g>
						</svg>
		
						<span>Assets</span>
		  
						<svg className="sidenav-v4__arrow-icon icon margin-left-auto" viewBox="0 0 20 20">
						  <g className="icon__group" fill="none" stroke="currentColor" strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round">
							<line x1="3" y1="3" x2="17" y2="17" />
							<line x1="17" y1="3" x2="3" y2="17" />
						  </g>
						</svg>
					  </a>
				
					  <ul className="sidenav-v4__sub-list">
						<li>
						  <a className="sidenav-v4__sub-link" href="assets.html">All Assets</a>
						</li>
						
						<li>
						  <a className="sidenav-v4__sub-link" href="new-asset.html">Upload</a>
						</li>
					  </ul>
					</li>
		
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="users.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<circle cx="10" cy="4" r="3" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></circle>
							<path d="M10 11a8 8 0 0 0-7.562 5.383A2 2 0 0 0 4.347 19h11.306a2 2 0 0 0 1.909-2.617A8 8 0 0 0 10 11z" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></path>
						  </g>
						</svg>
		
						<span>Users</span>
		  
						<svg className="sidenav-v4__arrow-icon icon margin-left-auto" viewBox="0 0 20 20">
						  <g className="icon__group" fill="none" stroke="currentColor" strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round">
							<line x1="3" y1="3" x2="17" y2="17" />
							<line x1="17" y1="3" x2="3" y2="17" />
						  </g>
						</svg>
					  </a>
				
					  <ul className="sidenav-v4__sub-list">
						<li>
						  <a className="sidenav-v4__sub-link" href="users.html">All Users</a>
						</li>
						
						<li>
						  <a className="sidenav-v4__sub-link" href="new-user.html">Add New</a>
						</li>
					  </ul>
					</li>
		
					<li className="sidenav-v4__item">
					  <a className="sidenav-v4__link js-sidenav-v4__link" href="settings.html">
						<svg className="sidenav-v4__icon icon" viewBox="0 0 20 20">
						  <g fill="currentColor">
							<path fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M14 2H6l-5 8 5 8h8l5-8-5-8z"></path>
							<circle cx="10" cy="10" r="3" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"></circle>
						  </g>
						</svg>
		
						<span>Settings</span>

						<svg className="sidenav-v4__arrow-icon icon margin-left-auto" viewBox="0 0 20 20">
						  <g className="icon__group" fill="none" stroke="currentColor" strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round">
							<line x1="3" y1="3" x2="17" y2="17" />
							<line x1="17" y1="3" x2="3" y2="17" />
						  </g>
						</svg>
					  </a>

					  <ul className="sidenav-v4__sub-list">
						<li>
						  <a className="sidenav-v4__sub-link" href="settings.html">Profile</a>
						</li>
						
						<li>
						  <a className="sidenav-v4__sub-link" href="password.html">Password</a>
						</li>
					  </ul>
					</li>
				  </ul>
				</nav>

				{/* mobile search */}
				<div className="padding-x-xs padding-bottom-sm hide@md">
				  <div className="padding-x-sm">
					<div className="search-input search-input--icon-left text-md">
					  <input className="search-input__input form-control" type="search" name="search-input" id="search-input" placeholder="Search..." aria-label="Search" />
					  <button className="search-input__btn">
						<svg className="icon" viewBox="0 0 20 20"><title>Submit</title><g fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"><circle cx="8" cy="8" r="6"/><line x1="12.242" y1="12.242" x2="18" y2="18"/></g></svg>
					  </button>
					</div>
				  </div>
				</div>
				{/* end mobile search */}
			  </div>
			  {/* end sidenav content */}

			  {/* start sidenav footer */}
			  <footer className="sidebar__footer border-top border-alpha padding-xs margin-top-auto position-sticky bottom-0 z-index-1">
				<div className="padding-x-xs flex items-center justify-between">
				  <label className="" htmlFor="select-theme">Theme:</label>
		
				  <div className="select text-sm" style={{"--select-icon-right-margin": "var(--space-xxs)", "--select-icon-size": "12px"}} >
					<select className="select__input btn btn--subtle padding-x-xxs padding-y-xxxs" name="select-theme" id="select-theme">
					  <option value="light">Light</option>
					  <option value="dark">Dark</option>
					  <option value="floral">Floral</option>
					  <option value="woody">Woody</option>
					  <option value="water">Water</option>
					</select>
					
					<svg className="icon select__icon" aria-hidden="true" viewBox="0 0 12 12"><polyline points="1 4 6 9 11 4" fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"/></svg>
				  </div>
				</div>
			  </footer>
			  {/* end sidenav footer */}
			</div>
		  </aside>
		)
	}
}

export default Nav;
