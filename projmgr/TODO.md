# React TODO's

## General

[ ] make main page component
[ ] make global variable file
[ ] separate app.js out to other files
[ ] make login/logout system
[X] page not found

## Users

[X] create
    [ ] fix random base64 crash when adding user (maybe only 1 char causes it?)
[ ] update
[ ] delete
[ ] read
[ ] make table
