package comments

import (
	"encoding/json"
	"fmt"
)

type Comment struct {
	Id      int    `json:"id"`
	ItemId  int    `json:"itemId"`
	UserId  int    `json:"userId"`
	Comment string `json:"comment"`
}

type Comments interface {
	GetId() int
	GetItemId() int
	GetUserId() int
	GetComment() string

	SetId() *Comment
	SetItemId() *Comment
	SetUserId() *Comment
	SetComment() *Comment

	ToString() string
}

func (c *Comment) GetId() int {
	return c.Id
}

func (c *Comment) GetItemId() int {
	return c.ItemId
}

func (c *Comment) GetUserId() int {
	return c.UserId
}

func (c *Comment) GetComment() string {
	return c.Comment
}

func (c *Comment) SetId(id int) *Comment {
	c.Id = id
	return c
}

func (c *Comment) SetItemId(id int) *Comment {
	c.ItemId = id
	return c
}

func (c *Comment) SetUserId(id int) *Comment {
	c.UserId = id
	return c
}

func (c *Comment) SetComment(comment string) *Comment {
	c.Comment = comment
	return c
}

func (c *Comment) ToJSON() string {
	byteArray, err := json.Marshal(c)
	if err != nil {
		fmt.Print(err)
	}
	return string(byteArray)
}

func FromJSON(s string) Comment {
	var cmt Comment
	err := json.Unmarshal([]byte(s), &cmt)
	if err != nil {
		fmt.Print(err)
	}
	return cmt
}
