package comments

import (
	"database/sql"
	"log"
)

func Create(r *sql.DB, cmmt *Comment) int {
	query := "INSERT INTO comments (item_id,user_id,comment) VALUES (?,?,?)"
	_, err := r.Exec(query, cmmt.GetItemId(), cmmt.GetUserId(), cmmt.GetComment())
	if err != nil {
		log.Fatal(err)
	}
	return 0
}

func Read(r *sql.DB, id int) Comment {
	query := "SELECT * FROM comments WHERE id=?"
	res := r.QueryRow(query, id)

	var cmmt Comment

	err := res.Scan(&cmmt.Id, &cmmt.ItemId, &cmmt.UserId, &cmmt.Comment)
	if err != nil {
		log.Fatal(err)
	}

	return cmmt
}

func ReadAll(r *sql.DB) []Comment {
	query := "SELECT * FROM comments"
	rows, err := r.Query(query)

	var comments []Comment

	for rows.Next() {
		var cmmt Comment
		if err := rows.Scan(&cmmt.Id, &cmmt.ItemId,
			&cmmt.UserId, &cmmt.Comment); err != nil {
			if err != nil {
				log.Fatal(err)
			}
		}
		comments = append(comments, cmmt)
	}

	if err != nil {
		log.Fatal(err)
	}
	return comments
}

func Update(r *sql.DB, cmmt *Comment, id int) {
	query := "UPDATE comments SET admin=?,name=?,description=? WHERE id=?"
	_, err := r.Exec(query, cmmt.GetItemId(), cmmt.GetUserId(), cmmt.GetComment(), id)
	if err != nil {
		log.Fatal(err)
	}
}

func Delete(r *sql.DB, id int) {
	query := "DELETE FROM comments WHERE id=?"
	_, err := r.Exec(query, id)
	if err != nil {
		log.Fatal(err)
	}
}
