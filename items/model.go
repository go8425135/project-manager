package item

import (
	"database/sql"
	"log"
)

func Create(r *sql.DB, itm *Item) int {
	query := "INSERT INTO project (project_id,type_id,state,priority,description) VALUES (?,?,?,?,?)"
	_, err := r.Exec(query, itm.GetProjectId(), itm.GetTypeId(),
		itm.GetState(), itm.GetPriority(), itm.GetDescription())
	if err != nil {
		log.Fatal(err)
	}
	return 0
}

func Read(r *sql.DB, id int) Item {
	query := "SELECT * FROM items WHERE project_id=?"
	res := r.QueryRow(query, id)

	var itm Item

	err := res.Scan(&itm.Id, &itm.ProjectId, &itm.TypeId, &itm.State, &itm.Priority, &itm.Description)
	if err != nil {
		log.Fatal(err)
	}

	return itm
}

func ReadAll(r *sql.DB) []Item {
	query := "SELECT * FROM items"
	rows, err := r.Query(query)

	var items []Item

	for rows.Next() {
		var itm Item
		if err := rows.Scan(&itm.Id, &itm.ProjectId, &itm.TypeId, &itm.State,
			&itm.Priority, &itm.Description); err != nil {
			if err != nil {
				log.Fatal(err)
			}
		}
		items = append(items, itm)
	}

	if err != nil {
		log.Fatal(err)
	}
	return items
}

func Update(r *sql.DB, itm *Item, id int) {
	query := "UPDATE items SET project_id=?,type_id=?,state=?,priority=?,description=? WHERE id=?"
	_, err := r.Exec(query, itm.GetProjectId(), itm.GetTypeId(),
		itm.GetState(), itm.GetPriority(), itm.GetDescription(), id)
	if err != nil {
		log.Fatal(err)
	}
}

func Delete(r *sql.DB, id int) {
	query := "DELETE FROM items WHERE id=?"
	_, err := r.Exec(query, id)
	if err != nil {
		log.Fatal(err)
	}
}
