package item

import (
	"fmt"
	"io"
	//"log"
	"encoding/json"
	"net/http"
	"projectmgr/configs"
	"projectmgr/db"
)

var DB = db.GetDB()

func ItemsForProj(w http.ResponseWriter, r *http.Request) {
	configs.SetHeaders(w)
	fmt.Printf("got /users request\n")

	req, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Println("Unable to process POST request.")
	}

	var itm Item
	err = json.Unmarshal(req, &itm)
	if err != nil {

		fmt.Println(err)
		fmt.Println("Bad request")
		str := "{'err':'Malformed Request'}"

		_, err = io.WriteString(w, str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	} else {

		display := (Read(DB, itm.GetProjectId()))
		str := display.ToJSON()

		err := json.NewEncoder(w).Encode(str)
		if err != nil {
			fmt.Println("Unable to process request.")
		}

	}

}
