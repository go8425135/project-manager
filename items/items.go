package item

import (
	"encoding/json"
	"fmt"
)

type Item struct {
	Id          int    `json:"id"`
	ProjectId   int    `json:"projectId"`
	TypeId      int    `json:"typeId"`
	State       int    `json:"state"`
	Priority    int    `json:"priority"`
	Description string `json:"description"`
}

type Items interface {
	GetId() int
	GetProjectId() int
	GetType() int
	GetState() int
	GetPriority() int
	GetDescription() string

	SetId() *Item
	SetProjectId() *Item
	SetType() *Item
	SetState() *Item
	SetPriority() *Item
	SetDescription() *Item

	ToString() string
}

func (i *Item) GetId() int {
	return i.Id
}

func (i *Item) GetProjectId() int {
	return i.ProjectId
}

func (i *Item) GetTypeId() int {
	return i.TypeId
}

func (i *Item) GetState() int {
	return i.State
}

func (i *Item) GetPriority() int {
	return i.Priority
}

func (i *Item) GetDescription() string {
	return i.Description
}

func (i *Item) SetId(id int) *Item {
	i.Id = id
	return i
}

func (i *Item) SetProjectId(projId int) *Item {
	i.ProjectId = projId
	return i
}

func (i *Item) SetTypeId(typeId int) *Item {
	i.TypeId = typeId
	return i
}

func (i *Item) SetState(stateId int) *Item {
	i.State = stateId
	return i
}

func (i *Item) SetPriority(priority int) *Item {
	i.Priority = priority
	return i
}

func (i *Item) SetDescription(descr string) *Item {
	i.Description = descr
	return i
}

func (i *Item) ToJSON() string {
	byteArray, err := json.Marshal(i)
	if err != nil {
		fmt.Print(err)
	}
	return string(byteArray)
}

func FromJSON(s string) Item {
	var itm Item
	err := json.Unmarshal([]byte(s), &itm)
	if err != nil {
		fmt.Print(err)
	}
	return itm
}
